/* 
 * NUMA感知调度算法：
 * 
 * 第一：
 *	识别出冷节点，将进程/线程还有内存映射到冷节点
 * 第二：
 *	动态监测冷热节点的变化情况，并再次将进程/线程和内存重新映射到冷节点上，保持冷节点的亲和性。
 */

伪代码：

define HOT 	0
define COLD 	1

node0_calories <- 0
node1_calories <- 0

LLC_entropy <- 0
IMC_entropy <- 0
QPI_entropy <- 0

llc_entroy_node0 <- 0
llc_entroy_node1 <- 0

llc_miss_node0 <- cache_misses_on_node(0)
llc_miss_node1 <- cache_misses_on_node(1)
LLC_entropy <- llc_miss_node1 - llc_miss_node0
if LLC_entropy > 0 then
	llc_entroy_node1++
	llc_entroy_node0--
else
	llc_entroy_node1--
	llc_entroy_node0++
	
imc_read_node0 <- imc_heat_on_node(0)
imc_read_node1 <- imc_heat_on_node(1)
IMC_entropy <- imc_read_node1 - imc_read_node0
if IMC_entropy > 0 then
	imc_entroy_node1++
	imc_entroy_node0--
else
	imc_entroy_node1--
	imc_entroy_node0++
	
qpi_data_node0 <- qpi_heat_on_node(0)
qpi_data_node1 <- qpi_heat_on_node(1)
QPI_entropy <- qpi_data_node1 - qpi_data_node0
if QPI_entropy > 0 then
	qpi_entroy_node1++
	qpi_entroy_node0--
else
	qpi_entroy_node1--
	qpi_entroy_node0++

node0_calories <- llc_entroy_node0 + imc_entroy_node0 + qpi_entroy_node0
node1_calories <- llc_entroy_node1 + imc_entroy_node1 + qpi_entroy_node1

calories_value <- node1_calories - node0_calories
if (calories_value > 0)
	current_cold <- node0
	stu_node0.flag <- COLD	
else
	current_cold <- node1
	stu_node1.flag <- COLD
	
pre_cold <- current_cold

current_cold <- find_current_cold()
if check_cold_if_change(current_cold, pre_cold) then
	current_cold <- cold_node
	
	cpu_id <- check_schedule_queue_length(cold_node)
	
	migrate_threads(cpu_id)
	migrate_pages(cold_node)
	


